package ai.maum.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Utils {

  public static String getStackTrace(Throwable e) {
    StringWriter msg = new StringWriter();
    e.printStackTrace(new PrintWriter(msg));
    return msg.toString();
  }
}
