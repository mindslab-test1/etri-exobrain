package ai.maum.exobrain;

import com.google.common.io.Files;
import com.google.protobuf.util.JsonFormat;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import maum.exobrain.wiki.Wiki;
import maum.exobrain.wiki.WikiData;
import maum.exobrain.wiki.WikiParagraph;
import maum.exobrain.wiki.WikiQuestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimilarityChecker {

  private static final Logger logger = LoggerFactory.getLogger(SimilarityChecker.class);

  private static void writeResult(File source, Wiki wiki) throws IOException {
    String sourceName = source.getCanonicalPath();
    File target = new File(source.getParent() + "/" + Files.getNameWithoutExtension(sourceName)
        + ".checked." + Files.getFileExtension(sourceName));
    FileWriter writer = new FileWriter(target);

    writer.write(JsonFormat.printer()
        .preservingProtoFieldNames()
//        .includingDefaultValueFields()
        .print(wiki));
    writer.close();
  }

  private static void checkTarget(String target) throws IOException {
    File targetFile = new File(target);
    FileReader reader = new FileReader(targetFile);
    Wiki.Builder wikiBuilder = Wiki.newBuilder();

    JsonFormat.parser().merge(reader, wikiBuilder);
    reader.close();

    int questionCount = 0;
    int lowerSimilarity = 0;
    int higherSimilarity = 0;
    float failScore = 0.0f;
    for (WikiData.Builder dataBuilder : wikiBuilder.getDataBuilderList()) {
      for (WikiQuestion.Builder questionBuilder : dataBuilder.getQuestionsBuilderList()) {
        questionCount++;
        String questionText = questionBuilder.getQuestionText();
        System.out.println(questionBuilder.getQuestionId() + ": "
            + questionBuilder.getQuestionText());
        for (WikiParagraph.Builder paragraphBuilder : questionBuilder.getParagraphsBuilderList()) {
          float similarity = NGram.getSimilarity(questionText, paragraphBuilder.getContext(), 2);
          paragraphBuilder.setQuestionSimilarity(similarity);
          if (similarity > 0.7f) {
            higherSimilarity++;
            paragraphBuilder.setSimilarityCompensation("-" + (similarity - 0.7f));
            failScore += similarity - 0.7f;
          } else if (similarity < 0.5f) {
            lowerSimilarity++;
            paragraphBuilder.setSimilarityCompensation("+" + (0.5f - similarity));
            failScore += 0.5f - similarity;
          }
//          System.out.println("  similarity: " + similarity);
        }
      }
    }

    System.out.println("\nTotal Data: " + wikiBuilder.getDataCount() + " checked.");
    System.out.println("Total Question: " + questionCount + " checked.");
    System.out.println("Similarity Error: " + (higherSimilarity + lowerSimilarity) + " found.");
    System.out.println("  Lower Similarity: " + lowerSimilarity + " found.");
    System.out.println("  Higher Similarity: " + higherSimilarity + " found.");
    System.out.println("  Average error: " + (failScore / (higherSimilarity + lowerSimilarity)));

    writeResult(targetFile, wikiBuilder.build());
  }


  public static void main(String[] args) throws IOException {
    System.out.println("wiki-similarity-checker start!");

    if (args.length > 0) {
//      checkTarget(args[0]);
      Wiki2ndData.combineData(args[0]);
    } else {
      System.out.println("\nWarning: Assign a JSON file to check!");
    }
  }
}
