package ai.maum.exobrain;

import java.util.ArrayList;
import java.util.List;

public class NGram {

  public static List<String> getNGrams(String sentence, int n) {
    List<String> ngrams = new ArrayList<>();
    String[] wordPhrases = sentence.split(" ");

    for (String wordPhrase : wordPhrases) {
      int lastIndex = wordPhrase.length() - n;

      if (lastIndex < 0) {
//        System.out.println("wordPhrase: " + wordPhrase);
        ngrams.add(wordPhrase);
      } else {
        for (int i = 0; i <= lastIndex; i++) {
//          System.out.println("wordPhrase: " + wordPhrase + " idx: " + i + "-" + (i + n));
          ngrams.add(wordPhrase.substring(i, i + n));
        }
      }
    }

    return ngrams;
  }

  public static float getSimilarity(List<String> ngrams1, List<String> ngrams2) {
    int same = 0;
    for (String ngram1 : ngrams1) {
      for (String ngram2 : ngrams2) {
        if (ngram1.compareTo(ngram2) == 0) {
          same++;
        }
      }
    }

    return (float) same / ngrams1.size();
  }

  public static float getSimilarity(String sentence1, String sentence2, int n) {
    List<String> ngrams1 = getNGrams(sentence1, n);
    List<String> ngrams2 = getNGrams(sentence2, n);

//    System.out.println("n-gram 1: " + ngrams1);
//    System.out.println("n-gram 2: " + ngrams2);

    return getSimilarity(ngrams1, ngrams2);
  }


}
