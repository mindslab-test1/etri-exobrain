package ai.maum.exobrain;

import com.google.protobuf.util.JsonFormat;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Objects;
import maum.exobrain.wiki2.Wiki;
import maum.exobrain.wiki2.WikiData;
import maum.exobrain.wiki2.WikiParagraph;
import maum.exobrain.wiki2.WikiQuestion;

public class Wiki2ndData {

  static int sameParagraph = 0;
  static int removedParagraph = 0;
  static int sameContext = 0;

  static void combineData(String targetPath) throws IOException {
    File target = new File(targetPath);

    if (!target.isDirectory()) {
      System.out.println(targetPath + " is not directory.");
      return;
    }

    System.out.println("combine Data from " + target.getName());

    Wiki.Builder builder = Wiki.newBuilder();

    builder.setCreator("MINDs Lab")
        .setVersion("MINDSLAB_2019");
    WikiData.Builder dataBuilder = builder.addDataBuilder();

    int count = 0;
    for (File document : Objects.requireNonNull(target.listFiles())) {
      dataBuilder.addQuestions(loadFile(document));
      count++;
//      if (count >= 5) {
//        break;
//      }
    }

    System.out.println("Same Paragraph: " + sameParagraph);
    System.out.println("Same Context: " + sameContext);
    System.out.println("Removed Paragraph: " + removedParagraph);
    System.out.println("Files: " + target.getName()
        + " ==> " + builder.getData(0).getQuestionsCount() + " questions loaded.");

    writeResult(target, builder.build());
  }

  static WikiQuestion loadFile(File document) throws IOException {
    String id = document.getName();
    String jsonString = new String(Files.readAllBytes(document.toPath()),
        StandardCharsets.UTF_8);

    jsonString = jsonString.replace("\"질문\":\"", "\"question_text\":\"")
        .replace("\"정답\":\"", "\"answer_reference\":\"")
        .replace("\"단락\":\"", "\"context_reference\":\"")
        .replace("\"검색결과\":", "\"paragraphs\":")
        .replace("\"단락번호\":\"", "\"id\":\"")
        .replace("\"문서제목\":\"", "\"title\":\"")
        .replace("\"단락내용\":\"", "\"context\":\"");

    WikiQuestion.Builder builder = WikiQuestion.newBuilder();
    JsonFormat.parser()
        .ignoringUnknownFields()
        .merge(jsonString, builder);

    builder.setQuestionId(id);
    builder.setContextReference("");
    ArrayList<Integer> sameList = new ArrayList<Integer>();
    for (int i = 0; i < builder.getParagraphsCount(); i++) {
      WikiParagraph.Builder paragraphBuilder = builder.getParagraphsBuilder(i);
      if (paragraphBuilder.getId().equals(id)) {
//        System.out.println("paragraph: " + paragraphBuilder.getId() + " is same.");
//        float similarity = NGram.getSimilarity(builder.getReferenceContext(),
//            paragraphBuilder.getContext(), 2);
//        System.out.println("similarity: " + similarity);
        sameParagraph++;
        if (paragraphBuilder.getContext().equals(builder.getContextReference())) {
//          System.out.println("context: " + paragraphBuilder.getId() + " is same.");
          sameContext++;
          sameList.add(i);
        }
      }
//      paragraphBuilder.addAnswers(WikiAnswer.newBuilder().build());
    }

//    System.out.println("found : " + sameList.size() + " same paragraphs.");
    for (int i = sameList.size() - 1; i > 0; i--) {
      builder.removeParagraphs(i);
      removedParagraph++;
    }

    System.out.print("File: " + document.getName()
        + " ==> " + builder.getParagraphsCount() + " paragraphs loaded.");
    if (sameList.size() > 1) {
      System.out.println(" ==> Removed: " + (sameList.size() - 1));
    } else {
      System.out.println();
    }

    return builder.build();
  }

  private static void writeResult(File source, Wiki wiki) throws IOException {
    File target = new File(source.getCanonicalFile().getParent()
        + "/" + source.getName() + ".json");

    FileWriter writer = new FileWriter(target);

    writer.write(JsonFormat.printer()
        .preservingProtoFieldNames()
//        .includingDefaultValueFields()
        .print(wiki));
    writer.close();

    System.out.println("Combined to " + target.getName());
  }
}
