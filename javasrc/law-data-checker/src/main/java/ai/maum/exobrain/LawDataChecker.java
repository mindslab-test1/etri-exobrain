package ai.maum.exobrain;

import com.google.common.io.Files;
import com.google.protobuf.util.JsonFormat;
import maum.exobrain.law.Law;
import maum.exobrain.law.LawData;
import maum.exobrain.law.LawParagraph;
import maum.exobrain.law.LawQuestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LawDataChecker {

  private static final Logger logger = LoggerFactory.getLogger(LawDataChecker.class);

  private static void writeResult(File source, Law law) throws IOException {
    String sourceName = source.getCanonicalPath();
    File target = new File(source.getParent() + "/" + Files.getNameWithoutExtension(sourceName)
      + ".checked." + Files.getFileExtension(sourceName));
    FileWriter writer = new FileWriter(target);

    writer.write(JsonFormat.printer()
      .preservingProtoFieldNames()
      .includingDefaultValueFields()
      .print(law));
    writer.close();
  }

  private static void checkTarget(String target) throws IOException {
    File targetFile = new File(target);
    FileReader reader = new FileReader(targetFile);
    Law.Builder lawBuilder = Law.newBuilder();

    JsonFormat.parser().merge(reader, lawBuilder);
    reader.close();

    int dataCount = 0;
    int paragraphCount = 0;
    int questionCount = 0;
    int wrongQuestionCount = 0;

    for (LawData.Builder dataBuilder : lawBuilder.getDataBuilderList()) {
      dataCount++;
      for (LawParagraph.Builder paragraphBuilder : dataBuilder.getParagraphsBuilderList()) {
        paragraphCount++;
        int levelCount = 0;
        StringBuilder levelBuilder = new StringBuilder();
        for (LawQuestion.Builder questionBuilder : paragraphBuilder.getQasBuilderList()) {
          questionCount++;
          levelBuilder.append(questionBuilder.getQuestionLevel());
          levelBuilder.append(' ');
          if (questionBuilder.getQuestionLevel().compareTo("상") == 0) {
            levelCount++;
          }
          if (questionBuilder.getQuestionLevel().compareTo("중") == 0) {
            levelCount++;
          }
          if (questionBuilder.getQuestionLevel().compareTo("하") == 0) {
            levelCount++;
          }

          if (questionBuilder.getAnswersCount() != 1) {
            System.out.println("paragraph context id: " + paragraphBuilder.getContextId());
            System.out.println("  QA levels: " + levelBuilder.toString());
            System.out.println("  Answer Count: " + questionBuilder.getAnswersCount());
          }

          if (questionBuilder.getAnswers(0).getText().isEmpty()) {
            System.out.println("paragraph context id: " + paragraphBuilder.getContextId());
            System.out.println("  QA levels: " + levelBuilder.toString());
            System.out.println("  Answer: " + questionBuilder.getAnswers(0).getText());
          }

          String question = questionBuilder.getQuestion();

          if (questionBuilder.getQuestion().contains("\n")) {
            if (questionBuilder.getQuestion().startsWith("\n")) {
              question = question.substring(1);
            }
            if (question.endsWith("\n")) {
              question = question.substring(0, question.length() - 1);
            }
          }

          if (!question.endsWith("?")) {
            question += '?';
          }
          questionBuilder.setQuestion(question);

          if (questionBuilder.getQuestion().contains("\n")) {
            wrongQuestionCount++;
            System.out.println(paragraphBuilder.getContextId() + " wrong question: " + questionBuilder.getQuestion());
          }

          if(questionBuilder.getLawName().contains("\n")) {
            questionBuilder.setLawName(questionBuilder.getLawName().replaceAll("\n", ""));
          }
        }

        if (levelBuilder.toString().compareTo("상 중 하 ") != 0) {
          System.out.println("paragraph context id: " + paragraphBuilder.getContextId());
          System.out.println("  QA levels: " + levelBuilder.toString());
        }

        if (levelCount != 3) {
          System.out.println("paragraph context id: " + paragraphBuilder.getContextId());
          System.out.println("  QA Count: " + paragraphBuilder.getQasCount());
        }
        if (paragraphBuilder.getQasCount() != 3) {
          System.out.println("paragraph context id: " + paragraphBuilder.getContextId());
          System.out.println("  QA Count: " + paragraphBuilder.getQasCount());
        }
      }
    }

    System.out.println("\nTotal Data: " + dataCount);
    System.out.println("Total Paragraph: " + paragraphCount);
    System.out.println("Total QAs: " + questionCount);
    System.out.println("Wrong Questions: " + wrongQuestionCount);

    writeResult(targetFile, lawBuilder.build());
  }

  public static void main(String[] args) throws IOException {
    System.out.println("law-data-checker start!");

    if (args.length > 0) {
      checkTarget(args[0]);
    } else {
      System.out.println("\nWarning: Assign a JSON file to check!");
    }
  }
}
