package ai.maum.utils;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GrpcClientPool<T extends GrpcClient> {

  private static final Logger logger = LoggerFactory.getLogger(GrpcClientPool.class);
  private static final long cleanUpPeriod = 10 * 60 * 1000L; // 10-minutes
  private final Object inReadyLock = new Object();
  private int defaultPoolSize = 3;
  private int maxPoolSize;
  private LinkedBlockingDeque<T> inReady;
  private LinkedBlockingQueue<T> inUse;
  private Timer cleanUpTimer;
  private Supplier<T> supplier;
  private String clientTypeName;
  private String clientHost;
  private int clientPort;
  private Executor clientExecutor;
  private boolean initialized = false;

  private TimerTask cleanUpTask = new TimerTask() {
    @Override
    public void run() {
      synchronized (inReadyLock) {
        int n;
        while (true) {
          n = inUse.size() + inReady.size();
          if (n <= defaultPoolSize) {
            break;
          }
          T client = inReady.peek();
          if (client == null || client.isApplicable()) {
            break;
          }
          inReady.remove(client);
          client.shutdown();
        }
        n = inUse.size() + inReady.size();
        for (; n < defaultPoolSize; n++) {
          T client = newClient();
          if (client != null) {
            inReady.add(client);
          }
        }
      }
    }
  };

  @SuppressWarnings("unused")
  private GrpcClientPool() {
  }

  public GrpcClientPool(Supplier<T> supplier) {
    this.supplier = supplier;
    String typeName = this.supplier.getClass().getSimpleName();
    clientTypeName = typeName.substring(0, typeName.indexOf('$'));
  }

  private T newClient() {
    try {
      T client = supplier.get();
      client.init(clientHost, clientPort, clientExecutor);
      return client;
    } catch (Exception e) {
      logger.error("Failed to get a new client<" + clientTypeName + "> .", e);
    }
    return null;
  }

  public String getClientTypeName() {
    return clientTypeName;
  }

  public GrpcClientPool<T> init(int maxPoolSize, String host, int port, Executor executor) {
    if (initialized) {
      logger.info(Utils.getStackTrace(
          new IllegalStateException("Pool<" + clientTypeName + "> is already initialized.")
      ));
      return this;
    }
    this.maxPoolSize = maxPoolSize;
    if (this.maxPoolSize < defaultPoolSize * 2) {
      defaultPoolSize = 1;
    }
    clientHost = host;
    clientPort = port;
    clientExecutor = executor;
    inReady = new LinkedBlockingDeque<>();
    inUse = new LinkedBlockingQueue<>();
    cleanUpTimer = new Timer();
    cleanUpTimer.schedule(cleanUpTask, 0, cleanUpPeriod);
    initialized = true;
    return this;
  }

  public void finish() {
    if (!initialized) {
      logger.info(Utils.getStackTrace(
          new IllegalStateException("Pool is not initialized.")
      ));
      return;
    }
    cleanUpTimer.cancel();
    inReady.forEach(T::shutdown);
    inUse.forEach(T::shutdown);
    clientTypeName = null;
    initialized = false;
  }

  public synchronized T getClient() throws IllegalStateException, InterruptedException {
    if (!initialized) {
      throw new IllegalStateException("Pool<" + clientTypeName + "> is not initialized.");
    }

    T client = inReady.pollLast();

    for (int i = 0; client == null && i < 10; i++) {
      client = inReady.pollLast(10, TimeUnit.MILLISECONDS);
      if (client == null) {
        if (inUse.size() < maxPoolSize) {
          client = newClient();
        }
      } else {
        if (!client.isApplicable()) {
          client.shutdown();
          client = null;
        }
      }
    }
    inUse.add(Objects.requireNonNull(client, "Failed to borrow a client<" + clientTypeName + ">."));
    return client;
  }

  public synchronized void releaseClient(T client) {
    if (!initialized) {
      logger.info(Utils.getStackTrace(
          new IllegalStateException("Pool<" + clientTypeName + "> is not initialized.")
      ));
      return;
    }

    try {
      Objects.requireNonNull(client, "Can't release null client<" + clientTypeName + ">.");
    } catch (Exception e) {
      logger.info(Utils.getStackTrace(e));
      return;
    }

    inUse.remove(client);
    if ((inUse.size() + inReady.size()) < maxPoolSize) {
      client.refreshLifetime();
      inReady.add(client);
    } else {
      client.shutdown();
    }
  }
}
