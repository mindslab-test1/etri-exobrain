package ai.maum.exobrain;

import java.io.File;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;

public class PhraseCounter {

  private static final String drama[] = {"완벽한", "자이언트", "줄리엣", "천만번", "카인과", "칼잡이"};


  public static void count(String target) {
    System.out.print("Target: " + target + "\n\n");
    try {
      int totalRowCount = 0;
      int totalPhraseCount = 0;
      int totalInvalidType = 0;

      int sentenceCount[] = {0, 0, 0, 0, 0, 0};
      int phraseCount[] = {0, 0, 0, 0, 0, 0};
      int invalidTypes[] = {0, 0, 0, 0, 0, 0};

      File targetFile = new File(target);
      Workbook workbook = XSSFWorkbookFactory.createWorkbook(targetFile, false);
      Sheet sheet = workbook.getSheetAt(0);
      int rows = sheet.getPhysicalNumberOfRows();

      System.out.println("ROWs: " + rows + "\n");
      for (int rowIndex = 1; rowIndex < rows; rowIndex++) {
        int dramaNum = -1;
        Row row = sheet.getRow(rowIndex); // 각 행을 읽어온다
        if (row != null) {
          totalRowCount++;

          int cells = row.getLastCellNum();
          String dramaTitle = row.getCell(0).getStringCellValue();

          for (int i = 0; i < 6; i++) {
            if (dramaTitle.startsWith(drama[i])) {
              dramaNum = i;
              break;
            }
          }

          if (dramaNum < 0) {
            System.out.println("Row-" + rowIndex + ": '" + dramaTitle + "' Invalid Drama.");
            System.exit(1);
          }

          Cell cell = row.getCell(1); // 셀에 담겨있는 값을 읽는다.
          if (cell.getCellType() != CellType.STRING) {
            System.out.println("Row-" + rowIndex + " Type is invalid.");
            totalInvalidType++;
            invalidTypes[dramaNum]++;
            continue;
          }
          String sentence = cell.getStringCellValue();
//          System.out.println("Row-" + rowIndex + ": " + sentence);
          int phrases = sentence.split(" ").length;

          totalPhraseCount += phrases;
//          phraseCount[dramaNum] += phrases;
          if (phrases > 3) {
            phraseCount[dramaNum] += phrases;
          }
          sentenceCount[dramaNum]++;

//          if (totalRowCount >= 15000) {
//            break;
//          }
        } else {
          System.out.println("Row-" + rowIndex + "is invalid.");
        }
      }

      System.out.println("\nTotal checked: " + totalRowCount);
      System.out.println("  Total Phrases: " + totalPhraseCount);
      System.out.println("  Total Invalid Types: " + totalInvalidType);
      for (int i = 0; i < 6; i++) {
        System.out.println("  Drama-" + i + " " + drama[i] + ": "
            + sentenceCount[i] + "," + phraseCount[i] + "," + invalidTypes[i] + ","
            + phraseCount[i] / sentenceCount[i]);
      }
      System.out.println();

    } catch (IOException | InvalidFormatException e) {
      e.printStackTrace();
    }
  }
}
