package ai.maum.exobrain;

import ai.maum.clients.NlpAnalysisClient;
import ai.maum.utils.GrpcClientPool;
import ai.maum.utils.ThreadPool;
import ai.maum.utils.Utils;
import com.google.common.io.Files;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import maum.brain.nlp.Document;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
1. excel 로드
2. 컬럼 오타 비교
3. 형태소 컬럼 비교
4. 결과 쓰기
 */

public class TagChecker {

  private static final Logger logger = LoggerFactory.getLogger(TagChecker.class);

  private static final boolean printCells = false;

  private static final String NLP_TARGET = "10.122.66.135";
  private static final short NLP_PORT = 9823;

  private static final int TAGGED_LIST_SIZE = 7;

  private static final int WORKER_ASSIGNED = 0;
  private static final int WORD_PHRASE = 1;
  private static final int GOLD_TAGGING = 2;
  private static final int GOLD_MODIFIED = 3;
  //  private static final int TYPE_COLUMN = 3;
  private static final int REASON_COLUMN = 4;
  private static final int A_TAGGING = 5;
  private static final int B_TAGGING = 6;

  private static CellStyle nullCellStyle;
  private static CellStyle workerErrorStyle;
  private static CellStyle taggingSuperiorStyle;
  private static CellStyle taggingInferiorStyle;

  private static void setStyles(Workbook workbook) {
    nullCellStyle = workbook.createCellStyle();
    nullCellStyle.setFillBackgroundColor(IndexedColors.TURQUOISE.getIndex());
    nullCellStyle.setFillPattern(FillPatternType.FINE_DOTS);

    workerErrorStyle = workbook.createCellStyle();
    workerErrorStyle.setFillBackgroundColor(IndexedColors.ROSE.getIndex());
    workerErrorStyle.setFillPattern(FillPatternType.LEAST_DOTS);

    taggingSuperiorStyle = workbook.createCellStyle();
    taggingSuperiorStyle.setFillBackgroundColor(IndexedColors.SEA_GREEN.getIndex());
    taggingSuperiorStyle.setFillPattern(FillPatternType.LEAST_DOTS);

    taggingInferiorStyle = workbook.createCellStyle();
    taggingInferiorStyle.setFillBackgroundColor(IndexedColors.RED1.getIndex());
    taggingInferiorStyle.setFillPattern(FillPatternType.LEAST_DOTS);
  }

  private static void showTaggings(final String fullSentence, final ArrayList arrTaggings) {
    System.out.println("t:\"" + arrTaggings.get(WORD_PHRASE) + "\" "
        + "g:\"" + arrTaggings.get(GOLD_TAGGING) + "\" "
        + "\" A:" + arrTaggings.get(A_TAGGING) + "\" "
        + " B:\"" + arrTaggings.get(B_TAGGING) + "\"");
  }

  private static void appendString(final Cell cell, final String value) {
    StringBuilder type = new StringBuilder(cell.getStringCellValue());
    if (type.length() != 0) {
      type.append(", ");
    }
    type.append(value);
    cell.setCellValue(type.toString());
  }

  private static String getNlpTaggedText(final String text) {
    NlpAnalysisClient client = null;
    try {
      client = NlpAnalysisClient.pool.getClient();
      final ListenableFuture<Document> future = client.analyze(text);
      Document document = future.get(10, TimeUnit.SECONDS);
//      logger.info("document: {}", JsonFormat.printer().print(document));
      List<String> tagged = client.getTaggedText(document);
      NlpAnalysisClient.pool.releaseClient(client);
      client = null;
      StringBuilder answer = new StringBuilder();
      for (String sentence : tagged) {
        answer.append(sentence);
        answer.append('\n');
      }
      answer.deleteCharAt(answer.length() - 1);

      return answer.toString();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.error(Utils.getStackTrace(e));
      System.exit(1);
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
      System.exit(1);
    }

    if (client != null) {
      NlpAnalysisClient.pool.releaseClient(client);
    }

    return null;
  }

  /**
   * detectNullError
   *
   * null인 셀을 검출한다.
   *
   * @param rowIndex 검출할 행 번호
   * @param arrTaggings 어절과 태깅된 결과들
   * @return null인 셀이 있을 경우 true를 리턴한다.
   */
  private static boolean detectNullCell(final int rowIndex,
      final ArrayList<String> arrTaggings,
      final ArrayList<Cell> arrCells) {
    boolean nullCellDetected = false;
    StringBuilder cellNullBuilder = new StringBuilder();

    for (int index = WORD_PHRASE; index <= B_TAGGING; index++) {
      if (index == GOLD_MODIFIED) {
        index = A_TAGGING;
      }
      if (cellNullBuilder.length() != 0) {
        cellNullBuilder.append(' ');
      }

      if (arrTaggings.get(index) == null || arrTaggings.get(index).isEmpty()) {
        nullCellDetected = true;
        cellNullBuilder.append('N');
        arrCells.get(index).setCellStyle(nullCellStyle);
      } else {
        cellNullBuilder.append('_');
      }
    }

    if (nullCellDetected) {
      System.out.println("ROW-" + rowIndex + ": Null Cell found."
          + " [" + cellNullBuilder.toString() + "]");
//      appendString(arrCells.get(TYPE_COLUMN), "빈값");
    }

    return nullCellDetected;
  }

  /**
   * detectWorkerError
   *
   * 작업자 실수를 검출한다.
   *
   * <pre>
   * 검출 조건
   *   1. fullSentence 에 WORD_PHRASE 가 포함되어 있지 않거나
   *   2. fullSentence 에 GOLD_TAGGING에서 태깅을 제거한 문장과 같지 않은 경우
   * </pre>
   *
   * @param rowIndex 검출할 행 번호
   * @param fullSentence 검출 대조를 윈한 원 문장
   * @param arrTaggings 어절과 태깅된 결과들
   * @return 작업자 실수 검출 시 true를 리턴한다.
   */
  private static boolean detectWorkerError(final int rowIndex,
      final String fullSentence,
      final ArrayList<String> arrTaggings,
      final ArrayList<Cell> arrCells) {
    boolean fullSentenceNotMatch;
    boolean taggingNotMatch;
    final String wordPhrase = arrTaggings.get(WORD_PHRASE);
    final String goldTagging = arrTaggings.get(GOLD_TAGGING);
    final String reverted = goldTagging.replaceAll("[/+A-Z]", "");
    String tagged = "~";

    fullSentenceNotMatch = !fullSentence.contains(wordPhrase);
    taggingNotMatch = wordPhrase.compareTo(reverted) != 0;

//    if (taggingNotMatch) {
//      tagged = getNlpTaggedText(wordPhrase).replaceAll("[/+A-Z]", "");
//      taggingNotMatch = reverted.compareTo(tagged) != 0;
//    }

    if (fullSentenceNotMatch || taggingNotMatch) {
      System.out.println("ROW-" + rowIndex + ": Worker Error suspicious. \""
          + fullSentence + "\" \"" + wordPhrase + "\" \"" + reverted + "\" \"" + tagged + "\"");
      arrCells.get(WORD_PHRASE).setCellStyle(workerErrorStyle);
      arrCells.get(GOLD_TAGGING).setCellStyle(workerErrorStyle);

//      appendString(arrCells.get(TYPE_COLUMN), "오타??");
//      if (fullSentenceNotMatch) {
      appendString(arrCells.get(REASON_COLUMN), "오타 확인");
//      }
    }

    return fullSentenceNotMatch || taggingNotMatch;
  }

  /**
   * detectDifferentTagging
   *
   * gold 컬럼과 A~F 컬럼의 형태소 태깅이 다른 경우를 검출한다.
   *
   * @param rowIndex 검출할 행 번호
   * @param arrTaggings 어절과 태깅된 결과들
   * @return 형태소 태깅이 다른 경우를 검출 시 true를 리턴한다.
   */
  private static boolean detectDifferentTagging(final int rowIndex,
      final ArrayList<String> arrTaggings,
      final ArrayList<Cell> arrCells) {
    int taggingDifferentCount = 0;
    String goldTagging = arrTaggings.get(GOLD_TAGGING);
    StringBuilder differentTaggingsBuilder = new StringBuilder();
    ArrayList<Boolean> arrDifferent = new ArrayList<>(B_TAGGING - A_TAGGING + 1);

    for (int index = A_TAGGING; index <= B_TAGGING; index++) {
      if (differentTaggingsBuilder.length() != 0) {
        differentTaggingsBuilder.append(' ');
      }
      if (goldTagging.compareTo(arrTaggings.get(index)) != 0) {
        arrDifferent.add(true);
        taggingDifferentCount++;
        differentTaggingsBuilder.append((char) ('A' + index - A_TAGGING));
      } else {
        arrDifferent.add(false);
        differentTaggingsBuilder.append('_');
      }
    }

    if (taggingDifferentCount > 0) {
      System.out.println("ROW-" + rowIndex + ": Different Taggings. \"" + goldTagging
          + "\" [" + differentTaggingsBuilder.toString() + "]");
      boolean isSuperior = taggingDifferentCount < 2;
      arrCells.get(GOLD_TAGGING)
          .setCellStyle(isSuperior ? taggingSuperiorStyle : taggingInferiorStyle);
      for (int index = A_TAGGING; index <= B_TAGGING; index++) {
        arrCells.get(index).setCellStyle(
            isSuperior != arrDifferent.get(index - A_TAGGING) ?
                taggingSuperiorStyle :
                taggingInferiorStyle);
      }
//      appendString(arrCells.get(TYPE_COLUMN), "태깅다름");
      if (!isSuperior) {
        appendString(arrCells.get(REASON_COLUMN), "gold 태깅이 틀림");
      }
    }

    return taggingDifferentCount > 0;
  }

  private static void writeResult(File source, Workbook workbook) throws IOException {
    String sourceName = source.getCanonicalPath();
    File target = new File(source.getParent() + "/" + Files.getNameWithoutExtension(sourceName)
        + ".checked." + Files.getFileExtension(sourceName));
    FileOutputStream fos = new FileOutputStream(target);

    System.out.println("Filtered out-file: " + target.getCanonicalPath());
    workbook.write(fos);
    fos.close();
  }

  private static void checkTarget(String target) {
    System.out.print("Target: " + target + "\n\n");
    try {
      int totalChecked = 0;
      int nullCellCount = 0;
      int workerErrorCount = 0;
      int differentTaggingCount = 0;
      int totalSuspicious = 0;

      ThreadPool.init(1, 5, true);
      final GrpcClientPool<NlpAnalysisClient> nlpPool = NlpAnalysisClient.pool
          .init(3, NLP_TARGET, NLP_PORT, ThreadPool.getExecutor());

      int fullSentenceCount = 0;
      boolean newFullSentence = false;
      boolean firstWordPhrase = false;
      String fullSentence = null;
      ArrayList<String> arrTaggings = new ArrayList<>(TAGGED_LIST_SIZE);
      ArrayList<Cell> arrCells = new ArrayList<>(TAGGED_LIST_SIZE);

      File targetFile = new File(target);
      Workbook workbook = XSSFWorkbookFactory.createWorkbook(targetFile, false);
      Sheet sheet = workbook.getSheetAt(0);
      int rows = sheet.getPhysicalNumberOfRows();

      setStyles(workbook);
      for (int i = 0; i < TAGGED_LIST_SIZE; i++) {
        arrTaggings.add(null);
        arrCells.add(null);
      }

      System.out.println("ROWs: " + rows + "\n");
      for (int rowIndex = 2; rowIndex <= rows; rowIndex++) {
        Row row = sheet.getRow(rowIndex - 1); // 각 행을 읽어온다
        if (row != null) {
          int cells = row.getLastCellNum();

          {
            Cell aTaggingCell = row.getCell(A_TAGGING);
            newFullSentence = aTaggingCell == null || aTaggingCell.getCellType() == CellType.BLANK;
            if (!newFullSentence) {
              newFullSentence = aTaggingCell.getCellType() == CellType.STRING
                  && aTaggingCell.getStringCellValue().isEmpty();
            }
            if (newFullSentence) {
              cells = 2;
            }
          }

//          System.out.print("ROW-" + rowIndex + ": " + cells + " Cells - ");
//          System.out.print("ROW-" + rowIndex + " - ");

          for (int columnIndex = 1; columnIndex < cells; columnIndex++) {
            // OX 컬럼 생략
            switch (columnIndex) {
//              case WORKER_ASSIGNED:
              case GOLD_MODIFIED:
              case 5:
              case 7:
              case 9:
              case 11:
              case 13:
              case 15:
                continue;
            }

            Cell cell = row.getCell(columnIndex); // 셀에 담겨있는 값을 읽는다.

            if (cell == null) {
              if (columnIndex > REASON_COLUMN) {
//              System.out.println("Column-" + columnIndex + ": {null}");
                continue;
              }
//              continue;
              cell = row.createCell(columnIndex);
              cell.setCellValue("");
            }

            String value = "";
//            System.out.print("Column-" + columnIndex + ": " + cell.getCellType().name() + " ");

            switch (cell.getCellType()) { // 각 셀에 담겨있는 데이터의 타입을 체크하고 해당 타입에 맞게 가져온다.
              case NUMERIC:
                value = cell.getNumericCellValue() + "";
                break;
              case STRING:
                value = cell.getStringCellValue();
                break;
              case BLANK:
                value = cell.getBooleanCellValue() + "";
                break;
              case ERROR:
                value = cell.getErrorCellValue() + "";
                break;
            }
//            System.out.println("\"" + value + "\"");

            if (newFullSentence) {
              fullSentence = value.substring(value.indexOf(':') + 2);
              for (int i = 0; i < TAGGED_LIST_SIZE; i++) {
                arrTaggings.set(i, null);
                arrCells.set(i, null);
              }
            } else {
              if (columnIndex > GOLD_TAGGING) {
                value = value.replaceAll("\\s+", "");
              }
              switch (columnIndex) {
                case WORD_PHRASE:
                case GOLD_TAGGING:
//                case TYPE_COLUMN:
                case REASON_COLUMN:
                  arrTaggings.set(columnIndex, value);
                  arrCells.set(columnIndex, cell);
                  break;
                case 6:
                  arrTaggings.set(A_TAGGING, value);
                  arrCells.set(A_TAGGING, cell);
                  break;
                case 8:
                  arrTaggings.set(B_TAGGING, value);
                  arrCells.set(B_TAGGING, cell);
                  break;
              }
            }
          }
          if (newFullSentence) {
            fullSentenceCount++;
            firstWordPhrase = true;
          } else {
            if (printCells) {
              if (firstWordPhrase) {
                if (fullSentenceCount > 1) {
                  System.out.println();
                }
                System.out.println("Full Sentence[" + fullSentenceCount + "]: \""
                    + fullSentence + "\"");
                firstWordPhrase = false;
              }
              showTaggings(fullSentence, arrTaggings);
            }

            totalChecked++;
            boolean isSuspicious = false;
            if (detectNullCell(rowIndex, arrTaggings, arrCells)) {
              nullCellCount++;
              isSuspicious = true;
            } else {
              if (detectWorkerError(rowIndex, fullSentence, arrTaggings, arrCells)) {
                workerErrorCount++;
                isSuspicious = true;
              }
              if (detectDifferentTagging(rowIndex, arrTaggings, arrCells)) {
                differentTaggingCount++;
                isSuspicious = true;
              }
            }
            if (isSuspicious) {
              totalSuspicious++;
            }
          }
        } else {
          System.out.println("is invalid.");
        }
      }

      nlpPool.finish();
      ThreadPool.finish(false);

      System.out.println("\nTotal checked: " + totalChecked);
      System.out.println("  Null Cell: " + nullCellCount);
      System.out.println("  Suspicious Worker Errors: " + workerErrorCount);
      System.out.println("  Different Taggings: " + differentTaggingCount);
      System.out.println("\nTotal Suspicious: " + totalSuspicious);
      System.out.println();
      writeResult(targetFile, workbook);

    } catch (IOException | InvalidFormatException e) {
      e.printStackTrace();
    }
  }

  private static void writeResult2(File source) throws IOException {
    String sourceName = source.getCanonicalPath();
    File target = new File((source.getParent() == null ? "./" : source.getParent()) + Files
        .getNameWithoutExtension(sourceName)
        + ".checked." + Files.getFileExtension(sourceName));
//    FileOutputStream fos = new FileOutputStream(target);

    System.out.println("Parent: " + source.getParent());
    System.out.println("Filtered out-file: " + target.getCanonicalPath());
//    fos.close();
  }

  public static void main(String[] args) throws IOException {
    System.out.println("exo-tag-checker start!");

    if (args.length > 0) {
//      writeResult2(new File(args[0]));
      checkTarget(args[0]);
//      PhraseCounter.count(args[0]);
    } else {
      System.out.println("\nWarning: Assign a Excel file to check!");
    }
  }
}
