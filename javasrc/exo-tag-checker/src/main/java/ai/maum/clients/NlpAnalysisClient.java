package ai.maum.clients;

import static maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.newFutureStub;

import ai.maum.utils.GrpcClient;
import ai.maum.utils.GrpcClientPool;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.Channel;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import maum.brain.nlp.Document;
import maum.brain.nlp.InputText;
import maum.brain.nlp.KeywordFrequencyLevel;
import maum.brain.nlp.MorphemeEval;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceFutureStub;
import maum.brain.nlp.Sentence;
import maum.common.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class NlpAnalysisClient extends GrpcClient<NaturalLanguageProcessingServiceFutureStub> {

  private static final Logger logger = LoggerFactory.getLogger(NlpAnalysisClient.class);

  /**
   * NLP Analysis Client Pool
   **/
  private static final Supplier<NlpAnalysisClient> supplier = NlpAnalysisClient::new;
  public static final GrpcClientPool<NlpAnalysisClient> pool = new GrpcClientPool<>(supplier);

  private NlpAnalysisClient() {
  }

  /**
   * NlpAnalysisClient Override
   **/
  @Override
  protected NaturalLanguageProcessingServiceFutureStub newStub(Channel underlyingChannel) {
    return newFutureStub(underlyingChannel);
  }


  /*
   * NlpAnalysisClient RPCs
   */

  /**
   * analyze
   *
   * @param text text
   * @return ListenableFuture<Document>
   */
  public ListenableFuture<Document> analyze(final String text) {
    InputText request = InputText.newBuilder()
        .setText(text)
        .setLang(LangCode.kor)
        .setSplitSentence(true)
        .setUseTokenizer(false)
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_ALL)
        .build();
    return stub.withCompression("gzip").analyze(request);
  }

  public List<String> getTaggedText(Document document) throws InvalidProtocolBufferException {
    List<String> tagged = new LinkedList<>();
    StringBuilder aggregate = new StringBuilder();

    for (Sentence sentence : document.getSentencesList()) {
      aggregate.setLength(0);
      for (MorphemeEval morphemeEval : sentence.getMorphEvalsList()) {
        aggregate.append(morphemeEval.getResult());
        aggregate.append(" ");
      }
      aggregate.deleteCharAt(aggregate.length() - 1);
      tagged.add(aggregate.toString());
    }

    return tagged;
  }
}

